package nl.ruben.bambam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BambamApplication {

	public static void main(String[] args) {
		SpringApplication.run(BambamApplication.class, args);
	}

}
