package nl.ruben.bambam.bambo;

import java.time.LocalDate;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Bambo {
	
	private Long id;

	private String name;
	
	private LocalDate start;
	
}
