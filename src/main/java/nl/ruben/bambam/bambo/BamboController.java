package nl.ruben.bambam.bambo;

import java.time.LocalDate;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BamboController {

	@GetMapping(path = "/bambo/{id}")
	public Bambo bambo(@PathVariable long id) {
		return Bambo.builder()
				.id(id)
				.name("Bimbi-" + id)
				.start(LocalDate.now())
				.build();
	}
	
}
