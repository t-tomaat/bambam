package nl.ruben.bambam.bambo;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest
public class BamboControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void bambo() throws Exception {
		mockMvc.perform(get("/bambo/1"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.name", equalTo("Bimbi-1")));
	}

}
